
all : fy.pat exceptions.txt unfound.txt
	wc -l exceptions.txt unfound.txt

# after generating the pattern file, make lists of all hyphenated words
# containing errors and all words containing unfound hyphenation points
#
exceptions.txt : data/errors.txt
	<data/errors.txt grep '[.]' | tr '*' - >exceptions.txt
unfound.txt : data/errors.txt
	<data/errors.txt grep -v '[.]' >unfound.txt
data/errors.txt : fy.pat
	<pattmp.6 grep '[.-]' > data/errors.txt

# generate the patterns
#
fy.pat : input.txt patgen/fy.pat.start patgen/fy.trans patgen/patgen_input
	patgen input.txt patgen/fy.pat.start fy.pat patgen/fy.trans <patgen/patgen_input

# remove breakpoints that need special treatment and add them to the
# exception list
#
input.txt : wurdlist.txt oanfang.hyp
	<wurdlist.txt \
		sed 's/ú-ch/ú=ch/g;s/ú-/ú/g;s/=/-/g' |\
		sed 's/aa-ch/aa=ch/g;s/aa-/aa/g;s/=/-/g' |\
		sed 's/-\([äëïöü]\)/\1/g' >input.txt
	cp oanfang.hyp fy.hyp
	<wurdlist.txt grep -- '-[äëïöü]' | grep -v "'" |\
		sed 's/-ä/{-}{a}{ä}/g' |\
		sed 's/-ë/{-}{e}{ë}/g' |\
		sed 's/-ï/{-}{i}{ï}/g' |\
		sed 's/-ö/{-}{o}{ö}/g' |\
		sed 's/-ü/{-}{u}{ü}/g' >>fy.hyp
	<wurdlist.txt grep 'ú-' | grep -v 'ú-ch' | grep -v "'" |\
		sed 's/ú-/{u-}{}{ú}/g' >>fy.hyp
	<wurdlist.txt grep 'aa-' | grep -v 'aa-ch' | grep -v "'" |\
		sed 's/aa-/{a-}{}{aa}/g' >>fy.hyp

